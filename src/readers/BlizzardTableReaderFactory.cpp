#include <readers/BlizzardTableReaderFactory.h>

namespace BlizzardDatabaseLib {
	namespace Reader {	
		std::shared_ptr<IBlizzardTableReader> BlizzardTableReaderFactory::For(std::shared_ptr<Stream::StreamReader> streamReader, Structures::VersionDefinition& versionDefinition, const std::string& formatSignature, const Structures::Build& build)
		{
			std::cout << "File Header Format: " << formatSignature << std::endl;

			if (Extension::String::Compare(formatSignature, std::string("WDC3")))
				return std::make_shared<WDC3TableReader>(streamReader, versionDefinition);
			if (Extension::String::Compare(formatSignature, std::string("WDBC")))
				return std::make_shared<WDBCTableReader>(streamReader, versionDefinition, build < Structures::Build("4.0.0.0000"));
		}
	}
}